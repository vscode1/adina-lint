# Change Log
All notable changes to the "adina-lint" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

- Initial release

## [0.3.0] - 2020-04-27

### Added

- Possibility to add header in the .in-file which will be shown in the Outline of the VSCode Explorer Tab. See Documentation / README.