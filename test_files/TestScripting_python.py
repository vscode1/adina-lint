
def getPoints(n=10):
    points = []
    point = "{0} {0} {0} {0} 0"

    for i in range(n):
        points.append(point.format(i+1))

    return "\n".join(points)


with open('./TestScripting.in', 'r') as file:
    f = file.read()
    f = f.replace('**** POINTS ****', getPoints())

    with open('./TestScripting_python.in', 'w') as file2:
        file2.write(f)
