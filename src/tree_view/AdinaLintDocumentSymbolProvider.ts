import * as vscode from 'vscode'

export class AdinaLintDocumentSymbolProvider implements vscode.DocumentSymbolProvider {
  public provideDocumentSymbols(
    document: vscode.TextDocument,
    token: vscode.CancellationToken
  ): Promise<vscode.DocumentSymbol[]> {
    return new Promise((resolve, reject) => {
      let symbols: vscode.DocumentSymbol[] = []
      let nodes: vscode.DocumentSymbol[] = []
      let headers: string[] = ['*####', '*###', '*##', '*#']

      function getItem(line: vscode.TextLine, index: number) {
        let symbol = new vscode.DocumentSymbol(
          line.text.replace(headers[index], ''),
          '',
          index % 2 ? 5 : 7,
          line.range,
          line.range
        )
        return symbol
      }

      lineLoop: for (var i = 0; i < document.lineCount; i++) {
        var line = document.lineAt(i)

        headerLoop: for (let j = 0; j < headers.length; j++) {
          if (line.text.startsWith(headers[j])) {
            let symbol = getItem(line, j)

            if (j === headers.length - 1) {
              nodes = [null, null, null, symbol]
              symbols.push(symbol)
              break headerLoop
            } else {
              if (nodes[j + 1]) {
                nodes[j] = symbol
                nodes[j + 1].children.push(symbol)
              } else {
                vscode.window.showErrorMessage(
                  `OUTLINE: Can't insert header at line ${i + 1}. Tried to insert header of level ${
                    headers[j]
                  }, but parent header ${headers[j + 1]} is missing.`
                )
                break lineLoop
              }
              break headerLoop
            }
          }
        }
      }
      resolve(symbols)
    })
  }
}
