import * as vscode from 'vscode'
import { exec } from 'child_process'
import { pathToAdinaRun } from '../settings/GlobalSettings'
import { getActiveDocumentPath } from '../globals/GlobalFunctions'
import { getCompileSettings } from '../settings/file_settings/CompileSettings'

export const runAdina = vscode.commands.registerCommand('adina-lint.runAdina', function (args) {
  try {
    function run(dir_path: string, file_name: string) {
      let shell_command = `cd "${dir_path}" && ${pathToAdinaRun()} -s ./${file_name} > ./0-ADINA.log`
      exec(shell_command, (error, stdout, stderr) => {
        if (error) {
          vscode.window.showErrorMessage('ERROR run ADINA: ' + error)
          vscode.window.showErrorMessage('ERROR run ADINA: ' + stderr)
          vscode.window.showErrorMessage('ERROR run ADINA: ' + stdout)
        } else {
          vscode.window.showInformationMessage('FINISHED run ADINA')
        }
      })
    }

    let compile = getCompileSettings(false)
    if (compile) {
      run(compile.dir_path, compile.file_name + '.in')
      return
    }

    let file_path = getActiveDocumentPath()
    if (file_path) {
      let dir_path = file_path.dir
      let file_name = file_path.base

      run(dir_path, file_name)
    } else {
      return null
    }
  } catch (e) {
    console.log(e)
  }
})
