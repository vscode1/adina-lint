import { Range, commands } from 'vscode'
import { toNumber, sortBy, toString } from 'lodash'
import { getActiveEditor } from '../globals/GlobalFunctions'

export const reindexRows = commands.registerCommand('adina-lint.reindexRows', function (args) {
  try {
    let editor = getActiveEditor()
    let selections = sortBy(editor.selections, 'start.line')
    let document = editor.document

    editor.edit((editBuilder) => {
      let startIndex = 0
      let maxStringLength = 0
      for (let i = 0; i < selections.length; i++) {
        let selection = selections[i]
        if (i === 0) {
          startIndex = toNumber(document.getText(selection))
          maxStringLength = toString(startIndex + selections.length).length
        }

        let newIndex = toString(startIndex + i).padStart(maxStringLength, ' ')

        editBuilder.replace(new Range(selection.start, selection.end), newIndex)
      }
    })
  } catch (e) {
    console.log(e)
  }
})
