import { Range, commands, TextLine, Selection, SelectionRange, Position, window } from 'vscode'
import { toNumber, orderBy, toString } from 'lodash'
import { getActiveEditor, lineIsComment, toggleLineComment } from '../globals/GlobalFunctions'
import { getCurrentHeaderRange } from '../globals/Header'

export const toggleLineCommentOfHeadersAndSubHeaders = commands.registerCommand(
  'adina-lint.toggleLineCommentOfHeadersAndSubHeaders',
  function (args) {
    try {
      let headerRange = getCurrentHeaderRange(true)

      toggleLineComment(headerRange.start, headerRange.end + 1, headerRange.allLinesAreCommented)
    } catch (e) {
      console.log(e)
    }
  }
)

function getHeaderText(level = 1) {
  let str = '****\n'
  str += '*'.append('#', level) + ` Level ${level}`
  str += '\n****'

  return str
}

export async function showQuickPickSelectLevel() {
  let i = 0
  const result = await window.showQuickPick(['Level 1', 'Level 2', 'Level 3', 'Level 4'], {
    placeHolder: 'Level 1,2,3 oder 4',
    //onDidSelectItem: (item) => window.showInformationMessage(`Focus ${++i}: ${item}`),
  })
  return toNumber(result.replace('Level ', ''))
}

export const insertHeader = commands.registerCommand('adina-lint.insertHeader', async function (args) {
  try {
    let level = await showQuickPickSelectLevel()

    let editor = getActiveEditor()
    let selection = editor.selection

    editor.edit((editorBuilder) => {
      editorBuilder.insert(new Position(selection.start.line, 0), getHeaderText(level))
    })
  } catch (e) {
    console.log(e)
  }
})
