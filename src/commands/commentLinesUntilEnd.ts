import { Range, commands, TextLine, Selection, SelectionRange, Position, window } from 'vscode'
import { toNumber, orderBy, toString } from 'lodash'
import { getActiveEditor, lineIsComment, toggleLineComment } from '../globals/GlobalFunctions'
import { getCurrentHeaderRange } from '../globals/Header'

export const commentLinesUntilEnd = commands.registerCommand('adina-lint.commentLinesUntilEnd', function (args) {
  try {
    let editor = getActiveEditor()
    let selection = editor.selection

    toggleLineComment(selection.start.line)
  } catch (e) {
    console.log(e)
  }
})
