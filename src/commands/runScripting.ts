import * as vscode from 'vscode'
import { exec } from 'child_process'
import { pathToPython } from '../settings/GlobalSettings'
import { get } from 'lodash'
import { getCompileSettings } from '../settings/file_settings/CompileSettings'

export const runScripting = vscode.commands.registerCommand('adina-lint.runScripting', function (args) {
  try {
    let compile = getCompileSettings()
    if (compile) {
      let shell_command = `cd "${compile.dir_path}" && ${pathToPython()} ./${compile.file_name}.py > ./0-SCRIPT.log`

      exec(shell_command, (error, stdout, stderr) => {
        if (error) {
          vscode.window.showErrorMessage('ERROR run Script: ' + error)
        } else if (stderr) {
          vscode.window.showErrorMessage('ERROR run Script: ' + stderr)
        } else {
          if (get(compile, 'runAdina', false)) {
            vscode.commands.executeCommand('adina-lint.runAdina')
          }

          vscode.window.showInformationMessage('FINISHED run Script')
        }
      })
    } else {
      return null
    }
  } catch (e) {
    console.log(e)
  }
})
