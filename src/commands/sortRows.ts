import { Range, commands, TextLine } from 'vscode'
import { toNumber, orderBy, toString } from 'lodash'
import { getActiveEditor } from '../globals/GlobalFunctions'
import { TextEditManager, DocumentLine, TextEditLineDelete, TextEditInsert } from 'text-edit-manager'

function sortRows(desc = false) {
  try {
    let editor = getActiveEditor()
    let manager = new TextEditManager(editor)
    let document = editor.document

    let selectionsSortedByUser = orderBy(
      editor.selections,
      (selection) => {
        return toNumber(document.getText(selection))
      },
      desc ? 'desc' : 'asc'
    )

    let firstLine: DocumentLine = null
    let textLines: string[] = []

    for (let selection of selectionsSortedByUser) {
      let line = manager.lineAt(selection.start.line)

      if (!firstLine || firstLine.getRange().start < line.getRange().start) {
        firstLine = line
      }

      textLines.push(line.getText())

      manager.addLineEdit(new TextEditLineDelete(line))
    }

    manager.addLineEdit(TextEditInsert.fromPrimitives(firstLine.getRange().start.line, 0, textLines.join('\n') + '\n'))

    manager.runEdits()
  } catch (e) {
    console.log(e)
  }
}

export const sortRowsAsc = commands.registerCommand('adina-lint.sortRowsAsc', function (args) {
  sortRows()
})

export const sortRowsDsc = commands.registerCommand('adina-lint.sortRowsDsc', function (args) {
  sortRows(true)
})
