import { splitInToColumns, lineIsComment, removeSpacesAroundEqual } from '../globals/GlobalFunctions'
import { isVariable } from './VariableFormatter'
import { spacesBetweenColumns, tabSize, numberOfColumnsOfSingeLine, numberOfColumns } from '../settings/GlobalSettings'
import { functionBodyStart } from './FunctionBodyFormatter'
import { DocumentLine, DocumentLineGroup, TextEditManager, TextEditLineDelete, TextEditInsert } from 'text-edit-manager'

let group: DocumentLineGroup = null

function sortInToColumns(lines: DocumentLine[]) {
  const res: Array<Array<string>> = []

  let items: Array<string> = []

  let firstLine: boolean = true
  lines.forEach((line) => {
    let columns: Array<string> = splitInToColumns(removeSpacesAroundEqual(line.getText()).replaceAll(',', ' ').trim())

    if (firstLine) {
      const firstParameter: number = columns.findIndex((str) => {
        return containsParameter(str)
      })

      const functionName = firstParameter !== -1 ? columns.slice(0, firstParameter).join(' ') : null

      if (functionName != null) {
        items.push(functionName)
      }

      for (let i = 0; i < columns.length; i++) {
        if (i >= firstParameter) {
          items.push(columns[i])
        }
      }

      firstLine = false
    } else {
      for (let i = 0; i < columns.length; i++) {
        items.push(columns[i])
      }
    }
  })

  let singleLine: boolean = items.length <= numberOfColumnsOfSingeLine() + 1
  if (singleLine) {
    items.forEach((item) => {
      res.push([item])
    })
  } else {
    let i = 0
    while (i < items.length) {
      if (i === 0) {
        res.push([items[i]])
        i++
        res.push([items[i]])
        i++
        for (let j = 0; j < numberOfColumns() - 1; j++) {
          res.push([''])
        }
      }
      for (let j = 0; j < numberOfColumns(); j++) {
        if (i < items.length) {
          res[j].push(items[i])
          i++
        } else {
          break
        }
      }
    }
  }

  return res
}

function getMostCharactersOfColumn(column: Array<string>) {
  let mostCharacters: number = 0
  let num: number = column.reduce(function (a, b) {
    return a.length > b.length ? a : b
  }).length

  if (mostCharacters < num) {
    mostCharacters = num
  }

  return mostCharacters
}

function formatText(row: Array<string>, mostCharacters: Array<number>, firstTime: boolean = false) {
  let text = ''

  for (let i = 0; i < row.length; i++) {
    const sp = i !== 0 ? spacesBetweenColumns() : 0

    let str = String(row[i]).padEnd(firstTime ? 0 : mostCharacters[i], ' ')
    text += str.padStart(str.length + sp, ' ')
  }

  text = text.trim()
  if (!firstTime) {
    text = ' '.repeat(tabSize()) + text
  }
  return text
}

function formatter(lines: DocumentLine[]) {
  const columns: Array<Array<string>> = sortInToColumns(lines)

  let mostCharacters: Array<number> = []
  columns.forEach((x) => {
    let arr = x.length > 1 ? x.slice(1) : x
    mostCharacters.push(getMostCharactersOfColumn(arr))
  })

  let text: string = ''
  for (let i = 0; i < columns[0].length; i++) {
    let row: Array<string> = []
    columns.forEach((col) => {
      if (i < col.length) {
        row.push(col[i])
      }
    })

    // res.push(replaceLine(lines[i], formatText(row, mostCharacters)))

    text += formatText(row, mostCharacters, i === 0) + (i === columns[0].length - 1 ? '\n' : ',\n')
  }

  lines.forEach((line) => {
    TextEditManager.addLineEdit(new TextEditLineDelete(line))
  })

  TextEditManager.addLineEdit(TextEditInsert.fromPrimitives(lines[0].getLineNumber(), 0, text))
}

export function functionFormatter(line: DocumentLine) {
  if (!group) {
    if (containsParameter(line.getText())) {
      group = new DocumentLineGroup()
      group.addLine(line)
    }
  } else {
    if (!containsParameter(line.getText())) {
      formatter(group.getLines())
      group = null
    } else {
      group.addLine(line)
    }
  }

  return !!group
}

// export function functionStart(line: string): boolean {
//     return line.search(/^(!=)?\s*?[a-zA-Z][\w-]+\s+(.*=)+/g) !== -1
// }
export function containsParameter(text: string): boolean {
  return text.indexOf('=') !== -1 && !isVariable(text) && !lineIsComment(text) && !functionBodyStart(text)
}
