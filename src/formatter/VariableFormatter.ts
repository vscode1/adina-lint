import { splitInToColumns } from '../globals/GlobalFunctions'
import { DocumentLine, TextEditManager, TextEditLineReplace } from 'text-edit-manager'

function formatText(text: string): string {
  let columns: Array<string> = splitInToColumns(text)

  columns[0] = 'PARAMETER'
  return columns.join(' ')
}

export function variableFormatter(line: DocumentLine) {
  if (isVariable(line.getText())) {
    line.setText(formatText(line.getText()))
    TextEditManager.addLineEdit(new TextEditLineReplace(line))
  }
}

export function isVariable(line: string): boolean {
  return line.trim().toLowerCase().indexOf('parameter') === 0
}
