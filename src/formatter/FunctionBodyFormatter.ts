import { tabSize, spacesBetweenColumns } from '../settings/GlobalSettings'
import { splitInToColumns, lineIsComment } from '../globals/GlobalFunctions'
import { has, forOwn, toNumber } from 'lodash'
import {
  DocumentLine,
  DocumentLineGroup,
  TextEditManager,
  TextEditLineReplace,
  TextEditLineDelete,
} from 'text-edit-manager'

let group: DocumentLineGroup = null

function getMostCharactersOfColumn(rowsInColumns: { [key: string]: string[] }) {
  let mostCharacters: { [key: string]: number } = {}

  forOwn(rowsInColumns, (columns, rowIndex) => {
    for (let i = 0; i < columns.length; i++) {
      if (!has(mostCharacters, i) || mostCharacters[i] < columns[i].length) {
        mostCharacters[i] = columns[i].length
      }
    }
  })

  return mostCharacters
}

function sortInToColumns(lines: DocumentLine[]): { [key: string]: string[] } {
  const columns: { [key: string]: string[] } = {}
  let lastLine = ''
  lines.forEach((line) => {
    if (!lineIsComment(line.getText())) {
      // Check if the line contains a line break. This line will contains a ,.
      if (line.getText().indexOf(',') >= 0) {
        // If line contains a line break, it will be saved and in the next loop it will be merged with the next line.
        lastLine = line.getText().replace(',', '    ')
        TextEditManager.addLineEdit(new TextEditLineDelete(line))
      } else {
        // It adds the last line if it's a line break otherwise lastLine will be a empty string.
        let column = splitInToColumns(lastLine + line.getText())

        for (let i = 0; i < column.length; i++) {
          if (has(columns, line.getLineNumber())) {
            columns[line.getLineNumber()].push(column[i])
          } else {
            columns[line.getLineNumber()] = [column[i]]
          }
        }

        lastLine = ''
      }
    }
  })

  return columns
}

function formatText(lineSortedInColumns: string[], mostCharacters: { [key: string]: number }) {
  let text = ''
  // Let's start with the first intend
  text += text.prepend(' ', tabSize())

  for (let i = 0; i < lineSortedInColumns.length; i++) {
    let column = lineSortedInColumns[i]
    // The first row don't need spaces
    const sp = i ? spacesBetweenColumns() : 0

    // Here it dicides if the entry is a number or a string. If it's is a number it will be right padded
    if (toNumber(column)) {
      // Because it's a number we add at the beginning some spaces for right padding
      let str = column.padStart(mostCharacters[i], ' ')
      // Then we add the space between the columns
      text += str.padStart(str.length + sp, ' ')
    } else {
      // Because it's a normal string/value we add at the end some spaces for left padding
      let str = column.padEnd(mostCharacters[i], ' ')
      // Then we add the space between the columns
      text += str.padStart(str.length + sp, ' ')
    }
  }

  return text
}

function formatter(group: DocumentLineGroup) {
  // Each row get sorted into columns and if the row is splited will be reduced to one row again.
  const rowsInColumns: { [key: string]: string[] } = sortInToColumns(group.getLines())

  // For each column we search a number of the most characters
  let mostCharacters: { [key: string]: number } = getMostCharactersOfColumn(rowsInColumns)

  group.getLines().forEach((line) => {
    if (lineIsComment(line.getText())) {
      line.setText(line.getText().trim().prepend(' ', tabSize()))
      TextEditManager.addLineEdit(new TextEditLineReplace(line))
    } else {
      if (has(rowsInColumns, line.getLineNumber())) {
        line.setText(formatText(rowsInColumns[line.getLineNumber()], mostCharacters))
        TextEditManager.addLineEdit(new TextEditLineReplace(line))
      }
    }
  })
}

export function functionBodyFormatter(new_line: DocumentLine) {
  if (!group) {
    if (functionBodyStart(new_line.getText())) {
      group = new DocumentLineGroup()
    }
  } else {
    if (functionBodyStop(new_line.getText())) {
      formatter(group)
      group = null
    } else {
      group.addLine(new_line)
    }
  }

  return !!group
}

export function functionBodyStart(text: string): boolean {
  return text.trim().indexOf('@CLEAR') === 0
}

export function functionBodyStop(text: string): boolean {
  return text.includes('@') && text.trim().length === 1
}
