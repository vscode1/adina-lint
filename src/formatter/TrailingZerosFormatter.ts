import { regexTrailingZeros, regexIsString } from '../globals/RegexFunctions'
import { DocumentLine, TextEditManager, TextEditLineReplace } from 'text-edit-manager'
import { lineIsComment } from '../globals/GlobalFunctions'

export function removeTrailingZeros(line: DocumentLine) {
  const replacement = '{{{0}}}'

  if (!lineIsComment(line.getText()) && line.getText().match(regexTrailingZeros())) {
    let matches = [...line.getText().matchAll(regexIsString())]
    let formattedText = line.getText().replace(regexIsString(), replacement)
    formattedText = regexTrailingZeros(formattedText, '$1$2')

    matches.forEach((match) => {
      formattedText = formattedText.replace(replacement, match[0])
    })

    line.setText(formattedText)
    TextEditManager.addLineEdit(new TextEditLineReplace(line))
  }
}
