// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode'
import { functionBodyFormatter } from './formatter/FunctionBodyFormatter'
import { variableFormatter } from './formatter/VariableFormatter'
import { functionFormatter } from './formatter/FunctionFormatter'
import {
  doFunctionFormatter,
  doVariableFormatter,
  doFunctionBodyFormatter,
  doRemoveTrailingZeros,
  viewOutline,
} from './settings/GlobalSettings'
import { AdinaLintDocumentSymbolProvider } from './tree_view/AdinaLintDocumentSymbolProvider'
import { runScripting } from './commands/runScripting'
import { runAdina } from './commands/runAdina'
import { TextEditManager, DocumentLine } from 'text-edit-manager'
import { removeTrailingZeros } from './formatter/TrailingZerosFormatter'
import { reindexRows } from './commands/reindexRows'
import { sortRowsAsc, sortRowsDsc } from './commands/sortRows'
import { toggleLineCommentOfHeadersAndSubHeaders, insertHeader } from './commands/header'
import { commentLinesUntilEnd } from './commands/commentLinesUntilEnd'

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "adina-lint" is now active!')

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  context.subscriptions.push(runScripting)
  context.subscriptions.push(runAdina)
  context.subscriptions.push(reindexRows)
  context.subscriptions.push(sortRowsAsc)
  context.subscriptions.push(sortRowsDsc)
  context.subscriptions.push(toggleLineCommentOfHeadersAndSubHeaders)
  context.subscriptions.push(insertHeader)
  context.subscriptions.push(commentLinesUntilEnd)

  if (viewOutline()) {
    context.subscriptions.push(
      vscode.languages.registerDocumentSymbolProvider(
        { scheme: 'file', language: 'in' },
        new AdinaLintDocumentSymbolProvider()
      )
    )
  }

  // vscode.window.createTreeView('outline', {
  //   treeDataProvider: new NodeDependenciesProvider(vscode.workspace.rootPath),
  // })

  vscode.languages.registerDocumentFormattingEditProvider('in', {
    provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
      try {
        const manager = new TextEditManager()

        let lineIterator = TextEditManager.makeLineIterator(document)
        for (var line of lineIterator) {
          if (doRemoveTrailingZeros()) {
            removeTrailingZeros(line)
          }

          if (doFunctionBodyFormatter()) {
            functionBodyFormatter(line)
          }
          if (doVariableFormatter()) {
            variableFormatter(line)
          }
          if (doFunctionFormatter()) {
            functionFormatter(line)
          }
        }
        return manager.getTextEdits()
      } catch (e) {
        console.log(e)
        return []
      }
    },
  })
}

// this method is called when your extension is deactivated
export function deactivate() {}

// Help https://vshaxe.github.io/vscode-extern/vscode/TextEdit.html?#delete
