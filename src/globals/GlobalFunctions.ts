import * as vscode from 'vscode'
import { get } from 'lodash'
import * as path from 'path'
import { lineIsHeader } from './Header'

export function getActiveEditor(): vscode.TextEditor {
  return get(vscode, 'window.activeTextEditor', null)
}

export function getActiveDocument(): vscode.TextDocument {
  return get(vscode, 'window.activeTextEditor.document', null)
}

export function getActiveDocumentPath() {
  const file_path = get(vscode, 'window.activeTextEditor.document.uri.fsPath', null)
  return path.parse(file_path)
}

export function lineIsComment(line: string) {
  return line.trim()[0] === '*' || line.trim().length === 0
}

export function toggleLineComment(startIndex: number, endIndex: number = null, allLinesAreCommented: boolean = null) {
  let editor = getActiveEditor()
  let document = editor.document

  if (!endIndex) {
    endIndex = document.lineCount
  }

  if (!allLinesAreCommented) {
    allLinesAreCommented = true
    for (let i = startIndex; i < endIndex; i++) {
      let line = document.lineAt(i)

      if (allLinesAreCommented) {
        allLinesAreCommented = lineIsComment(line.text)
      } else {
        break
      }
    }
  }

  editor.edit((editBuilder) => {
    for (let i = startIndex + 1; i < endIndex; i++) {
      let line = document.lineAt(i)
      if (line.text !== '****' && !lineIsHeader(line.text)) {
        if (allLinesAreCommented) {
          editBuilder.replace(line.range, line.text.replace('* ', ''))
        } else {
          editBuilder.insert(new vscode.Position(i, line.firstNonWhitespaceCharacterIndex), '* ')
        }
      }
    }
  })
}

export function splitInToColumns(text: string): Array<string> {
  const res: Array<string> = []

  let in_brackets: boolean = false
  let to_split: Array<number> = []
  for (let i = 0; i < text.length; i++) {
    let char = text[i]

    if (char === `'` || char === '"') {
      in_brackets = !in_brackets

      if (in_brackets) {
        in_brackets = text.indexOf(char, i) !== -1
      }
    } else if (char === ' ' && !in_brackets) {
      to_split.push(i)
    } else if (char === '*' && !in_brackets) {
      if (text[i - 1] !== ' ') {
        text = text.insertInTo(i, ' ')
        to_split.push(i)
      }
      break
    }
  }

  let old_index = 0
  for (let i = 0; i < to_split.length; i++) {
    const index = to_split[i]

    if (old_index !== index) {
      res.push(text.substring(old_index, index))
    }

    if (i === to_split.length - 1) {
      res.push(text.substring(index + 1))
    }

    old_index = index + 1
  }

  if (to_split.length === 0) {
    res.push(text)
  }

  return res
}

export function removeSpacesAroundEqual(text: string): string {
  return text.replace(/\s*?=\s*/g, '=')
}

export function addSpacesAroundEqual(text: string): string {
  return text.replace(/\s*?=\s*/g, ' = ')
}

// ************************************************************
// Systemerweiterungen

String.prototype.indexOfRegex = function (this: string, regex: string, startpos?: number) {
  let indexOf = this.substring(startpos || 0).search(regex)
  return indexOf >= 0 ? indexOf + (startpos || 0) : indexOf
}

String.prototype.insertInTo = function (this: string, pos: number, str: string) {
  return this.substring(0, pos) + str + this.substring(pos)
}

String.prototype.replaceAll = function (this: string, search: string, replacement: string): string {
  return this.split(search).join(replacement)
}

String.prototype.prepend = function (this: string, prepend: string, n: number = 1): string {
  let str = ''
  for (let i = 0; i < n; i++) {
    str += prepend
  }
  return str + this
}

String.prototype.append = function (this: string, append: string, n: number = 1): string {
  let str = ''
  for (let i = 0; i < n; i++) {
    str += append
  }
  return this + str
}
