import { TextEdit, TextLine, Position, Range } from 'vscode'
import { unset, map, values } from 'lodash'

export class DocumentManager {
  private documentLineEdits: DocumentLineEdit[]
  private static activeManager: DocumentManager

  constructor() {
    this.documentLineEdits = []
    DocumentManager.activeManager = this
  }

  addLineEdit(lineEdit: DocumentLineEdit) {
    if (lineEdit.description === 'insert') {
      this.documentLineEdits.push(lineEdit)
      return
    }
    const index = this.documentLineEdits.findIndex((item) => {
      return item.getLineNumber() === lineEdit.getLineNumber()
    })

    if (~index) {
      this.documentLineEdits[index] = lineEdit
    } else {
      this.documentLineEdits.push(lineEdit)
    }
  }

  static addLineEdit(lineEdit: DocumentLineEdit) {
    DocumentManager.getActiveManager().addLineEdit(lineEdit)
  }

  getTextEdits(): TextEdit[] {
    return map(this.documentLineEdits, (value) => {
      return value.getTextEdit()
    })
  }

  static getActiveManager() {
    return DocumentManager.activeManager
  }
}

export class DocumentLine {
  private static lines: { [key: string]: DocumentLine } = {}
  private lineNumber: number
  private text: string
  private rangeIncludingLineBreak: Range
  private range: Range
  private firstNonWhitespaceCharacterIndex: number

  constructor(
    lineNumber: number,
    text: string,
    range: Range = null,
    rangeIncludingLineBreak: Range = null,
    firstNonWhitespaceCharacterIndex: number = -1
  ) {
    this.lineNumber = lineNumber
    this.text = text
    this.range = range
    this.rangeIncludingLineBreak = rangeIncludingLineBreak
    this.firstNonWhitespaceCharacterIndex = firstNonWhitespaceCharacterIndex

    DocumentLine.lines[lineNumber] = this
  }

  getLineNumber() {
    return this.lineNumber
  }
  getText() {
    return this.text
  }
  setText(text: string) {
    this.text = text
  }
  getRangeIncludingLineBreak() {
    return this.rangeIncludingLineBreak
  }
  getRange() {
    return this.range
  }
  getFirstNonWhitespaceCharacterIndex() {
    return this.firstNonWhitespaceCharacterIndex
  }

  static fromLine(line: TextLine) {
    return new DocumentLine(
      line.lineNumber,
      line.text,
      line.range,
      line.rangeIncludingLineBreak,
      line.firstNonWhitespaceCharacterIndex
    )
  }
}

export class DocumentLineGroup {
  static groups: { [key: string]: DocumentLineGroup[] } = {}
  static idCounter: number = 0
  lines: { [key: string]: DocumentLine }
  id: number
  active: Boolean

  constructor() {
    this.lines = {}
    this.id = DocumentLineGroup.idCounter
    this.active = true
    DocumentLineGroup.idCounter++
  }

  addLine(line: DocumentLine) {
    this.lines[line.getLineNumber()] = line
  }

  getLines() {
    return values(this.lines)
  }

  removeLine(lineNumber: number) {
    unset(this.lines, lineNumber)
  }

  isActive() {
    return this.active
  }

  setActive() {
    this.active = true
  }

  isInactive() {
    return !this.isActive()
  }

  setInactive() {
    return (this.active = false)
  }
}

interface DocumentLineEdit {
  description: string
  getTextEdit: () => TextEdit
  getLineNumber: () => number
}

export class TextEditLineDelete implements DocumentLineEdit {
  description: string
  line: DocumentLine
  from: number
  to: number

  constructor(line: DocumentLine, from: number = null, to: number = null) {
    this.description = 'delete'
    this.line = line
    this.from = from
    this.to = to
  }

  getTextEdit() {
    if (this.to && this.from) {
      return TextEdit.delete(new Range(this.line.getLineNumber(), this.from, this.line.getLineNumber(), this.to))
    } else {
      return TextEdit.delete(this.line.getRangeIncludingLineBreak())
    }
  }

  getLineNumber() {
    return this.line.getLineNumber()
  }

  static removeStartingSpaces(line: DocumentLine): TextEditLineDelete {
    return new TextEditLineDelete(line, 0, line.getFirstNonWhitespaceCharacterIndex())
  }
}

export class DocumentLineReplace implements DocumentLineEdit {
  description: string
  line: DocumentLine
  from: number
  to: number

  constructor(line: DocumentLine, from: number = null, to: number = null) {
    this.description = 'replace'
    this.line = line
    this.from = from
    this.to = to
  }

  getTextEdit() {
    if (this.to && this.from) {
      return TextEdit.replace(
        new Range(new Position(this.line.getLineNumber(), this.from), new Position(this.line.getLineNumber(), this.to)),
        this.line.getText()
      )
    } else {
      return TextEdit.replace(this.line.getRange(), this.line.getText())
    }
  }

  getLineNumber() {
    return this.line.getLineNumber()
  }
}

export class TextEditLineInsert implements DocumentLineEdit {
  description: string
  lineNumber: number
  text: string
  pos: number

  constructor(lineNumber: number, text: string, pos: number) {
    this.description = 'insert'
    this.lineNumber = lineNumber
    this.text = text
    this.pos = pos
  }

  getTextEdit() {
    return TextEdit.insert(new Position(this.lineNumber, this.pos), this.text)
  }

  getLineNumber() {
    return this.lineNumber
  }
}
