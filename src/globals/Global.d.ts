declare interface String {
  indexOfRegex(this: string, regex: string, startpos: number): number
  insertInTo(this: string, pos: number, str: string): string
  replaceAll(this: string, search: string, replacement: string): string
  prepend(this: string, prepend: string, n: number): string
  append(this: string, append: string, n: number): string
}
