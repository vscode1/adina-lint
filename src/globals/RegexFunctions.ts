function regexOrString(regex: RegExp, target: string = null, replacement: string = null): RegExp | string {
  if (target && replacement) {
    const res = target.replace(regex, replacement)
    return res
  } else {
    return regex
  }
}

export function regexLineBreakWithComma(): RegExp
export function regexLineBreakWithComma(target: string, replacement: string): string
export function regexLineBreakWithComma(target: string = null, replacement: string = null): RegExp | string {
  return regexOrString(/,\s*\n\s*/g, target, replacement)
}

export function regexTrailingZeros(): RegExp
export function regexTrailingZeros(target: string, replacement: string): string
export function regexTrailingZeros(target: string = null, replacement: string = null): RegExp | string {
  return regexOrString(/(\d+\.0?)(\d*[1-9])?(0+)?/g, target, replacement)
}

export function regexIsString(): RegExp
export function regexIsString(target: string, replacement: string): string
export function regexIsString(target: string = null, replacement: string = null): RegExp | string {
  return regexOrString(/(')(.*)(')/g, target, replacement)
}
