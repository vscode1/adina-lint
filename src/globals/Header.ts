import { getActiveEditor, lineIsComment } from './GlobalFunctions'

export function lineIsHeader(line: string) {
  let level = null

  if (line.search(/^(\*#{1})(?!#)(.*)/g) !== -1) {
    level = 1
  } else if (line.search(/^(\*#{2})(?!#)(.*)/g) !== -1) {
    level = 2
  } else if (line.search(/^(\*#{3})(?!#)(.*)/g) !== -1) {
    level = 3
  } else if (line.search(/^(\*#{4})(?!#)(.*)/g) !== -1) {
    level = 4
  }

  return level
}

export function getHeader(curserLineIndex: number) {
  let editor = getActiveEditor()
  let document = editor.document

  let level: number = null
  let lineIndex: number = null

  for (let i = curserLineIndex; i >= 0; i--) {
    let line = document.lineAt(i)

    if (i === 0) {
      level = i
      lineIndex = i
      break
    } else if (lineIsHeader(line.text)) {
      level = lineIsHeader(line.text)
      lineIndex = i
      break
    }
  }

  return { level, lineIndex }
}

export function getCurrentHeader() {
  let editor = getActiveEditor()
  let selection = editor.selection

  return getHeader(selection.start.line)
}

export function getHeaderRange(curserLineIndex: number, includeSubHeaders = false) {
  let header = getHeader(curserLineIndex)

  let editor = getActiveEditor()
  let document = editor.document
  let lineCount = document.lineCount

  let startIndex = header.lineIndex
  let headerLevel = header.level
  let endIndex: number = null
  let allLinesAreCommented = true

  for (let i = startIndex; i < lineCount; i++) {
    let line = document.lineAt(i)

    if (!lineIsHeader(line.text) || (includeSubHeaders && lineIsHeader(line.text) > headerLevel)) {
      endIndex = line.range.start.line

      if (allLinesAreCommented) {
        allLinesAreCommented = lineIsComment(line.text)
      }
    } else if (i !== startIndex) {
      break
    }
  }

  return { start: startIndex, end: endIndex, allLinesAreCommented }
}

export function getCurrentHeaderRange(includeSubHeaders = false) {
  let editor = getActiveEditor()
  let selection = editor.selection

  return getHeaderRange(selection.start.line, includeSubHeaders)
}
