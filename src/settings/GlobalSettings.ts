import * as vscode from 'vscode'
const systemConfig = () => {
  return vscode.workspace.getConfiguration('adina-lint')
}

export const tabSize = () => {
  return vscode.workspace.getConfiguration().editor.tabSize
}
export const spacesBetweenColumns = () => {
  let s = systemConfig().spacesBetweenColumns
  return s >= 1 ? s : 2
}
export const numberOfColumns = () => {
  let n = systemConfig().numberOfColumns
  return n >= 1 ? n : 1
}
export const numberOfColumnsOfSingeLine = () => {
  let n = systemConfig().numberOfColumnsOfSingeLine
  return n >= 2 ? n : 2
}
export const doFunctionFormatter = () => {
  return systemConfig().functionFormatter
}
export const doVariableFormatter = () => {
  return systemConfig().variableFormatter
}
export const doFunctionBodyFormatter = () => {
  return systemConfig().functionBodyFormatter
}
export const pathToAdinaRun = () => {
  return systemConfig().pathToAdinaRun
}
export const pathToPython = () => {
  return systemConfig().pathToPython
}
export const viewOutline = () => {
  return systemConfig().viewOutline
}
export const doRemoveTrailingZeros = () => {
  return systemConfig().removeTrailingZeros
}
