import * as vscode from 'vscode'
import { getActiveDocument } from '../../globals/GlobalFunctions'
import { loadSetting } from './LoadSetting'
import * as path from 'path'

export const settingName = 'compile'

export function getCompileSettings(showErrorMessageIfNotFound = true) {
  let document = getActiveDocument()
  let setting = loadSetting(settingName)

  if (setting && document) {
    if (setting.length < 3) {
      vscode.window.showErrorMessage(
        'Can\'t compile the script, because of missing parameter in setting line. You have to write it in the following format: "**##compile/_anything/true" (or false)'
      )
      return false
    } else if (setting.length > 3) {
      vscode.window.showErrorMessage("Don't use / in your File name")
      return false
    } else if (!(setting[2].includes('true') || setting[2].includes('false'))) {
      vscode.window.showErrorMessage(
        'The setting line is wrong formatted. Setting parameter 2 is wrong formatted, has to be true or false! "**##compile/_anything/true" (or false)'
      )
      return false
    }

    let file_path_string = document.uri.fsPath
    var file_path = path.parse(file_path_string)
    var script_name = setting[1].trim()

    if (script_name[0] === '_') {
      script_name = file_path.name + script_name
    }

    return {
      dir_path: file_path.dir,
      file_name: script_name,
      runAdina: setting[2].includes('true'),
    }
  } else {
    if (showErrorMessageIfNotFound) {
      vscode.window.showWarningMessage(
        'To use the scripting you have to write --> **##compile/_python/true (or false) <-- at the beginning of the document. For mor information read the package description.'
      )
    }
    return null
  }
}
