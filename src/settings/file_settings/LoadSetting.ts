import { getActiveDocument } from '../../globals/GlobalFunctions'

export function loadSetting(str: string): string[] {
  const document = getActiveDocument()
  if (document) {
    for (let i = 0; i < document.lineCount; i++) {
      let line = document.lineAt(i)
      if (line.text.includes('**##' + str + '/')) {
        return line.text.split('/')
      }
    }
  }
  return null
}
