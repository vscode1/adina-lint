# Documentation Of The ADINA-Lint Extension

<div align="center">
<img height="150px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/logo/2005_logo.png"/>
</div>

> Set's your coding experience with ADINA into the another level!

[ADINA](http://www.adina.com/) is a priemer simulation software for advanced analyses. ADINA provides a GUI and also a impressive command language. To work easier with this command language this Visual Studio Code Extension provides headings to organize your code, code highlighting, formatting and useful commands.

- [Documentation Of The ADINA-Lint Extension](#documentation-of-the-adina-lint-extension)
  - [Headings](#headings)
    - [How To Insert a Header](#how-to-insert-a-header)
  - [Commands](#commands)
    - [How To Access The Commands](#how-to-access-the-commands)
    - [Reindex a Column](#reindex-a-column)
    - [Sort Rows By Values Of a Column](#sort-rows-by-values-of-a-column)
    - [Comment Or Uncomment All Selected Lines](#comment-or-uncomment-all-selected-lines)
    - [Comment Or Uncomment a Header Section](#comment-or-uncomment-a-header-section)
    - [Comment All Lines From Curren Position](#comment-all-lines-from-curren-position)
    - [Run ADINA Directly From VSCode](#run-adina-directly-from-vscode)
    - [Run Python Directly From VSCode](#run-python-directly-from-vscode)
      - [Important](#important)
      - [How To Use It](#how-to-use-it)
      - [Example](#example)
  - [Syntax Highlighting And Formatting](#syntax-highlighting-and-formatting)
    - [Parameters](#parameters)
      - [Settings](#settings)
    - [Functions](#functions)
      - [Settings](#settings-1)
    - [Function Body's](#function-bodys)
      - [Settings](#settings-2)
  - [Removing Of Trailing Zeros From Numbers](#removing-of-trailing-zeros-from-numbers)
  - [All Global Settings](#all-global-settings)
  - [All Settings](#all-settings)
  - [VSCode Useful Settings](#vscode-useful-settings)
    - [Format On Save](#format-on-save)

## Headings

To structure the .in-File, ADINA-Lint provides the possibility to add linked headings in four levels. Those will be shown in the _Outline_ section of the _Explorer_ tab (See image). This tab can be reached by clicking on the first symbol in the _Activity-Bar_ on the left side and then open the _OUTLINE_ slider.

> **_IMPORTANT:_** A sub heading needs always it's parent. Fore Example it is not possible to add `*## before *#` or `*### before *##`. This will end up in a error massage.

<div align="center">
<img height="200px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/images/ExampleOutline.png"/>
</div>

### How To Insert a Header

To insert some headers just go to the top right corner, press the three dot button `...` and select the _Insert Header_ command, or even easier just press the short key `ctrl+r h` (first press `ctrl+r` and then `h`). There are four levels are available.

- \*# Level 1
- \*## Level 2
- \*### Level 3
- \*#### Level 4

<div align="center">
<img height="400px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/InsertHeader.gif"/>
</div>

## Commands

### How To Access The Commands

Every Adina-Lint command can be found by clicking on the three dot button `...` in the top right corner. Furthermore most commands can be activated via a shortcut like `ctrl+r something` (first press `ctrl+r` and then press `something`). Adina-Lint uses always `ctrl+r` to get in the command modus and then `something` to start a specific command.

<div align="center">
<img height="400px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/HowToAccessTheCommands.gif"/>
</div>

### Reindex a Column

Adina-Lint makes it possible to reindex a given column. For this hold `alt+shift` pressed and select the column with the mouse. Then press `ctrl+r i` or select the command from the [list](#How-to-access-the-commands).

> The first entry of the column is the starting point of the reindexing. Fore example: If the first entry is 5, then the next entry will be 6 then 7 and so on.

<div align="center">
<img height="400px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/Reindexing.gif"/>
</div>

### Sort Rows By Values Of a Column

To sort the rows by the values of a column just mark the values. For this hold `alt+shift` pressed and select the column with the mouse. Then run the command `ctrl+r s` for ascending and `ctrl+r shift+s` for descending. It is also possible to select the commands from the [list](#How-to-access-the-commands).

<div align="center">
<img height="400px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/SortRows.gif"/>
</div>

### Comment Or Uncomment All Selected Lines

Actually this is normal a VSCode comment, but it is worth to be mentioned. In VSCode it is possible to select lines and comment or uncomment them by pressing `ctrl+shift+7` or select it from the menu `Edit/Toogle Line Comment`.

<div align="center">
<img height="600px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/ToggleLineComment.gif"/>
</div>

### Comment Or Uncomment a Header Section

Adina-Lint makes it possible to comment and uncomment a header section including the subsections. Just place the curser somewhere in the header section, then press `ctrl+r shift+7`. or select the commend from the [list](#How-to-access-the-commands). It will search upward for the next header row and comment or uncomment all including lines of this section.

<div align="center">
<img height="600px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/ToggleCommentHeaders.gif"/>
</div>

### Comment All Lines From Curren Position

To comment all lines below the curren curser position just press `ctrl+r ctrl+7` or select the commend from the [list](#How-to-access-the-commands).

<div align="center">
<img height="600px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/ToggleCommentEndOfFile.gif"/>
</div>

### Run ADINA Directly From VSCode

The extension provides a command to directly open a ADINA GUI and run the current .in-File. You can specify the path to the runnable of ADINA in the VSCode settings.

- You can use the red start button/icon in the upright corner like shown in the following picture.
- or you can use the shortcut `Ctrl+R` and after press `A`. As a donkey bridge you can think **R**un **A**DINA

<div align="center">
<img height="350px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/images/StartAdinaIcon.png"/>
</div>

### Run Python Directly From VSCode

The extension provides a command to directly run Python. You can specify the path in the settings.
The Idea is, that you have a .in-File that will be extended from python and save it in a new .in-file.

- In the original .in-File has to contain (somewhere at the top of the file) the setting line: `**##compile/_python/true`
  - The line hast to have at tow `/`. Those are the options.
  - The first part `**##compile` tells the extensions which settings are activated
  - The second `_python` tells the name of the script file (doesn't have to include python).
    - if you use `_` as fist character, then the current filename of the original .in-File will be extended by `_what ever you wrote`
    - It is recommended to remove the setting line with python in the new .in.File. Otherwise starting ADINA from the **generated** .in-File won't work. Starting ADINA from the **original** .in-File work anyway.
    - don't use file extensions like .py
  - Is the third part `true` ADINA will directly be started with the new compiled in.-File

#### Important

The new generated .in-File hast to have the same Name as the Python-File.

#### How To Use It

- You can use the gear button/icon in the upright corner like shown in the following picture.
- or you can use the shortcut `Ctrl+R` and after press `P`. As a donkey bridge you can think **R**un **P**ython

#### Example

1. filename.in
   - Contains the following setting `**##compile/_python/true`
   - run the scripting by button or shortcut
2. filename_python.py
   - will be called
   - Has to write the new/extended .in-file _filename_python.in_
3. filename_python.in
   - Will be called by ADINA

## Syntax Highlighting And Formatting

The code will look like as shown in the following pictures. This behavior can be changed in the settings of the Extension.

### Parameters

The Parameter will be highlighted and formatted as show in the following picture.

#### Settings

- adina-lint.variableFormatter
  - Default = true
  - Turns the variable formatter (PARAMTER) on or off

<div align="center">
<img height="150px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/images/ExampleParameter.png"/>
</div>

### Functions

This Extension marks the start of a function (in this case blue) and formatters the attributes in columns. This behavior can be changed in the settings off the Extension.

#### Settings

- adina-lint.functionFormatter
  - Default = true
  - Turns the function formatter on or off
- adina-lint.numberOfColumns
  - Default = 4
  - Sets the number of columns for the code formatting of a function. It have to be at least 1
- adina-lint.numberOfColumnsOfSingeLine
  - Default = 2
  - Sets the number of columns which are allowd to stay in a single line for the code formatting of a function. It have to be at least 2

<div align="center">
<img height="200px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/images/ExampleFunction.png"/>
</div>

### Function Body's

The Function Body is the part between @CLEAR and @. It is important that every @CLEAR ends with a @! The Function Body will be formatted as columns. This behavior can be changed in the settings of the Extension.

#### Settings

- adina-lint.functionBodyFormatter
  - Default = true
  - Turns the function body formatter on or off

<div align="center">
<img height="500px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/FormatFunctionBody.gif"/>
</div>

## Removing Of Trailing Zeros From Numbers

<div align="center">
<img height="500px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/RemoveTrailingZeros.gif"/>
</div>

## All Global Settings

- adina-lint.spacesBetweenColumns
  - Sets the number of spaces between columns for the code formatting of a function. It have to be at least 1

## All Settings

```json
{
  "adina-lint.functionBodyFormatter": true,
  "adina-lint.functionFormatter": true,
  "adina-lint.numberOfColumns": 4,
  "adina-lint.numberOfColumnsOfSingeLine": 2,
  "adina-lint.spacesBetweenColumns": 2,
  "adina-lint.variableFormatter": true,
  "adina-lint.pathToAdinaRun": "/opt/adina/95/tools/aui9.5",
  "adina-lint.pathToPython": "python3"
}
```

## VSCode Useful Settings

### Format On Save

To activate the format on save option go to `File/Preferences/Settings` select `TextEditor/Formatting` and turn on the option `Format On Save.` There are also other options like `Format On Paste` or `Format On Type` which can be selected.

<div align="center">
<img height="500px" src="https://gitlab.com/vscode1/adina-lint/-/raw/master/media/gifs/VSCodeSettingsFormatOnSave.gif"/>
</div>
